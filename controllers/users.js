const express = require('express');

//method url action

//GET /users/  list
function list(req, res, next) {
    res.send('Juan ');
  };

//GET /users/{id}  index
function index(req, res, next) {
  //req.params nos permite acceder a los parametros de la url
  //req.boy nos permite acceder a los parametros de la peticion
  const id = req.params.id;
  const ap = req.params.ap;
  res.send(`index Parametros => ${id}, ${ap}`);

};

//POST /users/  create
function create(req, res, next) {
  const name = req.body.name;
  const lastName= req.body.lastName;
  res.send(`create => parametros ${name}, ${lastName}`);
};

//PUT /users/{id}  replace
function replace(req, res, next) {
  res.send('respond with replace');
};

//PATCH /users/{id} update
function update(req, res, next) {
  res.send('respond with update');
};

//DELETE /users/{id} destroy
function destroy(req, res, next) {
  res.send('respond with destroy');
};


module.exports = { list, index, create, replace, update, destroy };