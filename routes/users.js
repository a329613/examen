var express = require('express');
var router = express.Router();
const controller = require('../controllers/users')

/* GET users listing. */
router.get('/', controller.list );

router.get('/:id/:ap', controller.index );

router.post('/', controller.create );

router.put('/:id', controller.replace );

router.patch('/', controller.update );

router.delete('/', controller.destroy );

module.exports = router;
